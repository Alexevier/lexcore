/* Copyright (C) 2024 alexevier <alexevier@proton.me>

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution. */

/* LexCore
	aLexevier's Core
 */

/* this is lexcore's public api header. */
#ifndef lexcore_h
#define lexcore_h

#include<stdint.h>

/* attributes */
#ifdef __GNUC__
#	define LEXCORE_ATTRIB_ALWAYSINLINE __attribute__((always_inline))
#	define LEXCORE_ATTRIB_NOTHROW __attribute__((nothrow))
#	define LEXCORE_ATTRIB_RETURNS_NONNULL __attribute__((returns_nonnull))
#	define LEXCORE_ATTRIB_VISIBILITY_DEFAULT __attribute__((visibility("default")))
#else
#	define LEXCORE_ATTRIB_ALWAYSINLINE
#	define LEXCORE_ATTRIB_NOTHROW
#	define LEXCORE_ATTRIB_RETURNS_NONNULL
#	define LEXCORE_ATTRIB_VISIBILITY_DEFAULT
#endif

/* c++ compatibility */
#ifdef __cplusplus
#	define LEXCOREAPI_ATTRIB_EXTERN extern "C"
#	define LEXCOREAPI_ATTRIB_RESTRICT
#else
#	define LEXCOREAPI_ATTRIB_EXTERN extern
#	define LEXCOREAPI_ATTRIB_RESTRICT restrict
#endif

/* api */
#define LEXCOREAPI LEXCOREAPI_ATTRIB_EXTERN LEXCORE_ATTRIB_VISIBILITY_DEFAULT
#define LEXCOREAPI_INLINEFN static inline LEXCORE_ATTRIB_ALWAYSINLINE

/* a lexcore boolean value.
	8bit unsigned integer.
	value should be 0 or 1 but do not rely on true being 1. */
typedef unsigned char LexcoreBool;

/* lexcore enum value.
	16 bit unsigned integer.
 */
typedef uint16_t LexcoreEnum;

/* get lexcore's version in format "X.X.X". */
LEXCORE_ATTRIB_NOTHROW LEXCORE_ATTRIB_RETURNS_NONNULL
LEXCOREAPI const char *lexcoreVersion(void);

#define LEXCORE_TRUE 1
#define LEXCORE_FALSE 0

/* lexcore enum values */
#define LEXCORE_NONE 0x0000u
#define LEXCORE_ERROR 0xFF00u
#define LEXCORE_UNKNOWN 0xFFFFu

#endif
