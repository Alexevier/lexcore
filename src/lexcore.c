/* Copyright 2024 alexevier <alexevier@proton.me>
licensed under the zlib license <https://www.zlib.net/zlib_license.html> */

#include<lexcore.h>

#ifndef PROJECT_VERSION
#	error PROJECT_VERSION is not defined
#endif

const char *lexcoreVersion(void){
	static const char version[] = PROJECT_VERSION;
	return version;
}
